﻿using System.Collections.Generic;
using WarehouseBLL.ViewModels;

namespace WarehouseBLL.Interfaces
{
    public interface ICategoryService
    {
        IEnumerable<CategoryVM> GetCategories();
        CategoryVM GetCategory(int id);
        void CreateCategory(CategoryVM categoryVm);
        void UpdateCategory(CategoryVM categoryVm);
        void DeleteCategory(int id);
    }
}