﻿using System.Collections.Generic;
using WarehouseBLL.ViewModels;

namespace WarehouseBLL.Interfaces
{
    public interface ICustomerService
    {

        IEnumerable<CustomerVM> GetCustomers();
        CustomerVM GetCustomer(int id);
        void CreateCustomer(CustomerVM customerVm);
        void UpdateCustomer(CustomerVM customerVm);
        void DeleteCustomer(int id);
    }
}