﻿using System;
using System.Collections.Generic;
using WarehouseBLL.ViewModels;

namespace WarehouseBLL.Interfaces
{
    public interface IInInvoiceService
    {
        IEnumerable<InInvoiceVM> GetIncomeInvoices();
        InInvoiceVM GetIncomeInvoice(int id);
        void CreateIncomeInvoice(Guid EmployeeId, InInvoiceVM inInvoiceVm);
        void UpdateIncomeInvoice(InInvoiceVM inInvoiceVm);
        void DeleteIncomeInvoice(int id);
    }
}