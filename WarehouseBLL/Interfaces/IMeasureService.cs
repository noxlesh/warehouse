﻿using System.Collections.Generic;
using WarehouseBLL.ViewModels;

namespace WarehouseBLL.Interfaces
{
    public interface IMeasureService
    {
        IEnumerable<MeasureVM> GetMeasures();
        MeasureVM GetMeasure(int id);
        void CreateMeasure(MeasureVM measureVm);
        void UpdateMeasure(MeasureVM measureVm);
        void DeleteMeasure(int id);
    }
}