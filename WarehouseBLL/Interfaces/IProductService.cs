﻿using System.Collections.Generic;
using WarehouseBLL.ViewModels;

namespace WarehouseBLL.Interfaces
{
    public interface IProductService
    {
        IEnumerable<ProductVM> GetProducts();
        ProductVM GetProduct(int id);
        void CreateProduct(ProductCuVm productCuVm);
        void UpdateProduct(ProductCuVm productCuVm);
        void DeleteProduct(int id);
    }
}