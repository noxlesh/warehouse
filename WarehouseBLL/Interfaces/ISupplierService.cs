﻿using System.Collections.Generic;
using WarehouseBLL.ViewModels;

namespace WarehouseBLL.Interfaces
{
    public interface ISupplierService
    {
        IEnumerable<SupplierVM> GetSuppliers();
        SupplierVM GetSupplier(int id);
        void CreateSupplier(SupplierVM supplierVm);
        void UpdateSupplier(SupplierVM supplierVm);
        void DeleteSupplier(int id);
    }
}