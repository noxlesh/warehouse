﻿using System.Collections.Generic;
using System.Linq;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;

namespace WarehouseBLL.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category> _categoyRepo;

        public CategoryService(IRepository<Category> cr)
        {
            _categoyRepo = cr;
        }
        
        public IEnumerable<CategoryVM> GetCategories()
        {
            if (!_categoyRepo.GetAll().Any()) return null;
            List<CategoryVM> cl = new List<CategoryVM>();
            foreach (Category category in _categoyRepo.GetAll())
            {
                cl.Add(new CategoryVM
                {
                    Id = category.Id,
                    Name = category.Name,
                    Products = category.Products
                });
            }

            return cl;
        }

        public CategoryVM GetCategory(int id)
        {
            Category category = _categoyRepo.Get(id);
            if (category == null) return null;
            return new CategoryVM
            {
                Id = category.Id,
                Name = category.Name,
                Products = category.Products
            };
        }

        public void CreateCategory(CategoryVM categoryVm)
        {
            _categoyRepo.Create(new Category
            {
                Id = categoryVm.Id,
                Name = categoryVm.Name,
                Products = categoryVm.Products
            });
        }

        public void UpdateCategory(CategoryVM categoryVm)
        {
            _categoyRepo.Update(new Category
            {
                Id = categoryVm.Id,
                Name = categoryVm.Name,
                Products = categoryVm.Products
            });
        }

        public void DeleteCategory(int id)
        {
            _categoyRepo.Delete(id);
        }
    }
}