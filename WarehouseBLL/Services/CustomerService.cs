﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;

namespace WarehouseBLL.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<CustomerPhone> _customerPhoneRepo;

        public CustomerService(IRepository<Customer> cr,IRepository<CustomerPhone> cpr)
        {
            _customerRepo = cr;
            _customerPhoneRepo = cpr;
        }
        
        public IEnumerable<CustomerVM> GetCustomers()
        {
            if (!_customerRepo.GetAll().Any()) return null;
            List<CustomerVM> cl = new List<CustomerVM>();
            foreach (Customer customer in _customerRepo.GetAll().Include(s => s.CustomerPhones))
            {
                cl.Add(new CustomerVM
                {
                    Id = customer.Id,
                    Name = customer.Name,
                    Address = customer.Address,
                    CustomerPhones = GetCustomerPhoneVms(customer)
                });
            }
            return cl;
        }

        public CustomerVM GetCustomer(int id)
        {
            Customer customer = _customerRepo.Get(id);
            if (customer == null) return null;
            return new CustomerVM
            {
                Id = customer.Id,
                Name = customer.Name,
                Address = customer.Address,
                CustomerPhones = GetCustomerPhoneVms(customer)
            };
        }

        public void CreateCustomer(CustomerVM customerVm)
        {   
            var customer = new Customer
            {
                Name = customerVm.Name,
                Address = customerVm.Address,
            };
            _customerRepo.Create(customer);
            
            foreach (var customerPhoneVm in customerVm.CustomerPhones)
            {
                _customerPhoneRepo.Create(new CustomerPhone
                {
                    Phone = customerPhoneVm.Phone,
                    CustomerId = customer.Id
                });
            }
        }

        public void UpdateCustomer(CustomerVM customerVm)
        {   
            var customer = new Customer
            {
                Name = customerVm.Name,
                Address = customerVm.Address
            };
            _customerRepo.Update(customer);
            
            foreach (var customerPhoneVm in customerVm.CustomerPhones)
            {
                if (customerPhoneVm.Created)
                {
                    _customerPhoneRepo.Create(new CustomerPhone
                    {
                        Phone = customerPhoneVm.Phone,
                        CustomerId = customer.Id
                    });
                }

                if (customerPhoneVm.Deleted)
                {
                    _customerPhoneRepo.Delete(customerPhoneVm.Id);
                }

                if (customerPhoneVm.Updated)
                {
                    _customerPhoneRepo.Update(new CustomerPhone
                    {
                        Id = customerPhoneVm.Id,
                        Phone = customerPhoneVm.Phone,
                        CustomerId = customer.Id
                    });
                }
            }
        }

        public void DeleteCustomer(int id)
        {
            _customerRepo.Delete(id);
        }
        
        private List<CustomerPhoneVM> GetCustomerPhoneVms(Customer customer)
        {
            List<CustomerPhoneVM> cvml = new List<CustomerPhoneVM>();
            foreach (var customerPhone in customer.CustomerPhones)
            {
                cvml.Add(new CustomerPhoneVM
                {
                    Id = customerPhone.Id,
                    Phone = customerPhone.Phone
                });
            }
            
            return cvml;
        }
    }
}