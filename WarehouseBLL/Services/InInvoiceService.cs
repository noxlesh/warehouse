﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;

namespace WarehouseBLL.Services
{
    public class InInvoiceService : IInInvoiceService
    {
        private IRepository<InInvoice> _inInvoiceRepo;
        private IRepository<InItem> _inItemRepo;

        public InInvoiceService(IRepository<InInvoice> iir, IRepository<InItem> iitr)
        {
            _inInvoiceRepo = iir;
            _inItemRepo = iitr;
        }

        public IEnumerable<InInvoiceVM> GetIncomeInvoices()
        {
            var iiq = _inInvoiceRepo.GetAll()
                .Include(i => i.InItems)
                .ThenInclude(ii => ii.Product)
                .Include(i => i.Supplier)
                .Include(i => i.Employee);
            if (!iiq.Any()) return null;
            List<InInvoiceVM> sl = new List<InInvoiceVM>();
            foreach (var inInvoice in iiq)
            {
                
                sl.Add(new InInvoiceVM
                {
                    Id = inInvoice.Id,
                    Supplier = inInvoice.Supplier,
                    Employee = inInvoice.Employee,
                    IncomeItems = GetInItemVms(inInvoice)
                });
            }
            return sl;
        }

        public InInvoiceVM GetIncomeInvoice(int id)
        {
            var inInvoice = _inInvoiceRepo.GetAll()
                .Where(ii => ii.Id == id)
                .Include(i => i.InItems)
                .ThenInclude(ii => ii.Product)
                .Include(i => i.Supplier)
                .Include(i => i.Employee)
                .First();
            if (inInvoice == null) return null;
            return new InInvoiceVM
            {
                Id = inInvoice.Id,
                Supplier = inInvoice.Supplier,
                Employee = inInvoice.Employee,
                IncomeItems = GetInItemVms(inInvoice)
            };
        }

        public void CreateIncomeInvoice(Guid employeeId, InInvoiceVM inInvoiceVm)
        {
            InInvoice inInvoice = new InInvoice
            {
                Date = DateTime.Now,
                EmployeeId = employeeId,
                Supplier = inInvoiceVm.Supplier
            };
            _inInvoiceRepo.Create(inInvoice);
            foreach (var incomeItem in inInvoiceVm.IncomeItems)
            {
                _inItemRepo.Create(new InItem
                {
                    Product = incomeItem.Product,
                    Count = incomeItem.Count,
                    BuyPrice = incomeItem.BuyPrice
                });
            }
            
        }

        public void UpdateIncomeInvoice(InInvoiceVM inInvoiceVm)
        {
            InInvoice inInvoice = new InInvoice
            {
                Date = inInvoiceVm.Date,
                Employee = inInvoiceVm.Employee,
                Supplier = inInvoiceVm.Supplier
            };
            _inInvoiceRepo.Update(inInvoice);
            
            foreach (var inItemWm in inInvoiceVm.IncomeItems)
            {
                if (inItemWm.Created)
                {
                    _inItemRepo.Create(new InItem
                    {
                        Product = inItemWm.Product,
                        Count = inItemWm.Count,
                        BuyPrice = inItemWm.BuyPrice
                    });
                }

                if (inItemWm.Deleted)
                {
                    _inItemRepo.Delete(inItemWm.Id);
                }

                if (inItemWm.Updated)
                {
                    _inItemRepo.Update(new InItem
                    {
                        Id = inItemWm.Id,
                        Product = inItemWm.Product,
                        Count = inItemWm.Count,
                        BuyPrice = inItemWm.BuyPrice
                    });
                }
            }
        }

        public void DeleteIncomeInvoice(int id)
        {
            _inInvoiceRepo.Delete(id);
        }
        
        private List<InItemWM> GetInItemVms(InInvoice inInvoice)
        {
            List<InItemWM> iil = new List<InItemWM>();
            foreach (var inItemWm in inInvoice.InItems)
            {
                iil.Add(new InItemWM
                {
                    Id = inItemWm.Id,
                    Product = inItemWm.Product,
                    Count = inItemWm.Count,
                    BuyPrice = inItemWm.BuyPrice
                });
            }
            
            return iil;
        }
    }
}