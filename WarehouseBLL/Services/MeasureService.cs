﻿using System.Collections.Generic;
using System.Linq;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;

namespace WarehouseBLL.Services
{
    public class MeasureService : IMeasureService
    {
        private readonly IRepository<Measure> _measureRepo;

        public MeasureService(IRepository<Measure> mr)
        {
            _measureRepo = mr;
        }
        
        public IEnumerable<MeasureVM> GetMeasures()
        {
            if (!_measureRepo.GetAll().Any()) return null;
            List<MeasureVM> ml = new List<MeasureVM>();
            foreach (Measure measure in _measureRepo.GetAll())
            {
                ml.Add(new MeasureVM {Id = measure.Id, Name = measure.Name});
            }
            return ml;
        }

        public MeasureVM GetMeasure(int id)
        {
            Measure measure = _measureRepo.Get(id);
            if (measure == null) return null;
            return new MeasureVM {Id = measure.Id, Name = measure.Name};

        }

        public void CreateMeasure(MeasureVM measureVm)
        {
            _measureRepo.Create(new Measure{Name = measureVm.Name});
        }

        public void UpdateMeasure(MeasureVM measureVm)
        {
            _measureRepo.Update(new Measure{Name = measureVm.Name});
        }

        public void DeleteMeasure(int id)
        {
            _measureRepo.Delete(id);
        }
    }
    
}