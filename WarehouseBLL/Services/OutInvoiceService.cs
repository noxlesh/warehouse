﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;

namespace WarehouseBLL.Services
{
    public class OutInvoiceService : IOutInvoiceService
    {
        private IRepository<OutInvoice> _outInvoiceRepo;
        private IRepository<OutItem> _outItemRepo;

        public OutInvoiceService(IRepository<OutInvoice> oir, IRepository<OutItem> oitr)
        {
            _outInvoiceRepo = oir;
            _outItemRepo = oitr;
        }

        public IEnumerable<OutInvoiceVM> GetOutcomeInvoices()
        {
            var oiq = _outInvoiceRepo.GetAll()
                .Include(i => i.OutItems)
                .ThenInclude(ii => ii.Product)
                .Include(i => i.Customer)
                .Include(i => i.Employee);
            if (!oiq.Any()) return null;
            List<OutInvoiceVM> sl = new List<OutInvoiceVM>();
            foreach (var outInvoice in oiq)
            {
                
                sl.Add(new OutInvoiceVM
                {
                    Id = outInvoice.Id,
                    Customer = outInvoice.Customer,
                    Employee = outInvoice.Employee,
                    OutItems = GetOutItemVms(outInvoice)
                });
            }
            return sl;
        }

        public OutInvoiceVM GetOutcomeInvoice(int id)
        {
            var outInvoice = _outInvoiceRepo.GetAll()
                .Where(ii => ii.Id == id)
                .Include(i => i.OutItems)
                .ThenInclude(ii => ii.Product)
                .Include(i => i.Customer)
                .Include(i => i.Employee)
                .First();
            if (outInvoice == null) return null;
            return new OutInvoiceVM
            {
                Id = outInvoice.Id,
                Customer = outInvoice.Customer,
                Employee = outInvoice.Employee,
                OutItems = GetOutItemVms(outInvoice)
            };
        }

        public void CreateOutcomeInvoice(Guid employeeId, OutInvoiceVM outInvoiceVm)
        {
            OutInvoice inInvoice = new OutInvoice
            {
                Date = DateTime.Now,
                EmployeeId = employeeId,
                Customer = outInvoiceVm.Customer
            };
            _outInvoiceRepo.Create(inInvoice);
            foreach (var incomeItem in outInvoiceVm.OutItems)
            {
                _outItemRepo.Create(new OutItem
                {
                    Product = incomeItem.Product,
                    Count = incomeItem.Count,
                    SellPrice = incomeItem.SellPrice
                });
            }
            
        }

        public void UpdateOutcomeInvoice(OutInvoiceVM outInvoiceVm)
        {
            OutInvoice outInvoice = new OutInvoice
            {
                Date = outInvoiceVm.Date,
                Employee = outInvoiceVm.Employee,
                Customer = outInvoiceVm.Customer
            };
            _outInvoiceRepo.Update(outInvoice);
            
            foreach (var outItemWm in outInvoiceVm.OutItems)
            {
                if (outItemWm.Created)
                {
                    _outItemRepo.Create(new OutItem
                    {
                        Product = outItemWm.Product,
                        Count = outItemWm.Count,
                        SellPrice = outItemWm.Product.UnitPrice
                    });
                }

                if (outItemWm.Deleted)
                {
                    _outItemRepo.Delete(outItemWm.Id);
                }

                if (outItemWm.Updated)
                {
                    _outItemRepo.Update(new OutItem
                    {
                        Id = outItemWm.Id,
                        Product = outItemWm.Product,
                        Count = outItemWm.Count,
                        SellPrice = outItemWm.SellPrice
                    });
                }
            }
        }

        public void DeleteOutcomeInvoice(int id)
        {
            _outInvoiceRepo.Delete(id);
        }
        
        private List<OutItemVM> GetOutItemVms(OutInvoice outInvoice)
        {
            List<OutItemVM> oil = new List<OutItemVM>();
            foreach (var outItemWm in outInvoice.OutItems)
            {
                oil.Add(new OutItemVM
                {
                    Id = outItemWm.Id,
                    Product = outItemWm.Product,
                    Count = outItemWm.Count,
                    SellPrice = outItemWm.SellPrice
                });
            }
            
            return oil;
        }
    }
}