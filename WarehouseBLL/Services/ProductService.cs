﻿using System.Collections.Generic;
using System.Linq;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;

namespace WarehouseBLL.Services
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Product> _productRepo;
        private readonly IRepository<Measure> _measureRepo;
        
        public ProductService(IRepository<Product> pr,IRepository<Measure> mr)
        {
            _productRepo = pr;
            _measureRepo = mr;
        }
        
        public IEnumerable<ProductVM> GetProducts()
        {
            if (!_productRepo.GetAll().Any()) return null;
            List<ProductVM> pl = new List<ProductVM>();
            foreach (Product product in _productRepo.GetAll())
            {
                var measureName = _measureRepo.Get(product.MeasureId).Name;
                pl.Add(new ProductVM {
                    Id = product.Id,
                    Name = product.Name,
                    MeasureName = measureName,
                    Notation = product.Notation,
                    UnitPrice = product.UnitPrice
                });
            }
            return pl;
        }

        public ProductVM GetProduct(int id)
        {
            var product = _productRepo.Get(id);
            if (product == null) return null;
            var measureName = _measureRepo.Get(product.MeasureId).Name;
            return new ProductVM
            {
                Id = product.Id,
                Name = product.Name,
                MeasureName = measureName,
                Notation = product.Notation,
                UnitPrice = product.UnitPrice
            };
        }

        public void CreateProduct(ProductCuVm productCuVm)
        {
            _productRepo.Create(new Product
            {
                Name = productCuVm.Name,
                MeasureId = productCuVm.MeasureId,
                Notation = productCuVm.Notation,
                UnitPrice = productCuVm.UnitPrice,
                CategoryId = productCuVm.CategoryId
            });
        }

        public void UpdateProduct(ProductCuVm productCuVm)
        {
            _productRepo.Update(new Product
            {
                Name = productCuVm.Name,
                MeasureId = productCuVm.MeasureId,
                Notation = productCuVm.Notation,
                UnitPrice = productCuVm.UnitPrice,
                CategoryId = productCuVm.CategoryId
            });
        }

        public void DeleteProduct(int id)
        {
            _productRepo.Delete(id);
        }
    }
}