﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;

namespace WarehouseBLL.Services
{
    public class SupplierService : ISupplierService
    {
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<SupplierPhone> _supplierPhoneRepo;

        public SupplierService(IRepository<Supplier> supplierRepo, IRepository<SupplierPhone> supplierPhoneRepo)
        {
            _supplierRepo = supplierRepo;
            _supplierPhoneRepo = supplierPhoneRepo;
        }

        public IEnumerable<SupplierVM> GetSuppliers()
        {
            if (!_supplierRepo.GetAll().Any()) return null;
            List<SupplierVM> sl = new List<SupplierVM>();
            foreach (Supplier supplier in _supplierRepo.GetAll().Include(s => s.SupplierPhones))
            {
                
                sl.Add(new SupplierVM
                {
                    Id = supplier.Id,
                    Name = supplier.Name,
                    Address = supplier.Address,
                    SupplierPhones = GetSupplierPhoneVms(supplier)
                    
                });
            }
            return sl;
        }

        public SupplierVM GetSupplier(int id)
        {
            Supplier supplier = _supplierRepo.Get(id);
            if (supplier == null) return null;
            return new SupplierVM
            {
                Id = supplier.Id,
                Name = supplier.Name,
                Address = supplier.Address,
                SupplierPhones = GetSupplierPhoneVms(supplier)
            };
        }

        public void CreateSupplier(SupplierVM supplierVm)
        {
            var supplier = new Supplier
            {
                Name = supplierVm.Name,
                Address = supplierVm.Address
            };
            _supplierRepo.Create(supplier);
            
            foreach (var supplierPhoneVm in supplierVm.SupplierPhones)
            {
                _supplierPhoneRepo.Create(new SupplierPhone
                {
                    Phone = supplierPhoneVm.Phone,
                    SupplierId = supplier.Id
                });
            }
        }

        public void UpdateSupplier(SupplierVM supplierVm)
        {
            var supplier = new Supplier
            {
                Name = supplierVm.Name,
                Address = supplierVm.Address
            };
            _supplierRepo.Update(supplier);
            
            foreach (var supplierPhoneVm in supplierVm.SupplierPhones)
            {
                if (supplierPhoneVm.Created)
                {
                    _supplierPhoneRepo.Create(new SupplierPhone
                    {
                        Phone = supplierPhoneVm.Phone,
                        SupplierId = supplier.Id
                    });
                }

                if (supplierPhoneVm.Deleted)
                {
                    _supplierPhoneRepo.Delete(supplierPhoneVm.Id);
                }

                if (supplierPhoneVm.Updated)
                {
                    _supplierPhoneRepo.Update(new SupplierPhone
                    {
                        Id = supplierPhoneVm.Id,
                        Phone = supplierPhoneVm.Phone,
                        SupplierId = supplier.Id
                    });
                }
            }
        }

        public void DeleteSupplier(int id)
        {
            _supplierRepo.Delete(id);
        }

        private List<SupplierPhoneVM> GetSupplierPhoneVms(Supplier supplier)
        {
            List<SupplierPhoneVM> svml = new List<SupplierPhoneVM>();
            foreach (var supplierPhone in supplier.SupplierPhones)
            {
                svml.Add(new SupplierPhoneVM
                {
                    Id = supplierPhone.Id,
                    Phone = supplierPhone.Phone
                });
            }
            
            return svml;
        }
    }
}