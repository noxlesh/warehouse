﻿using System.Collections.Generic;
using WarehouseDAL.Entities;

namespace WarehouseBLL.ViewModels
{
    public class CategoryVM : BaseVM
    {
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}