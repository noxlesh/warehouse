﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WarehouseBLL.ViewModels
{
    public class CustomerVM : BaseVM
    {
        [Required,MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(128)]
        public string Address { get; set; }
        public IEnumerable<CustomerPhoneVM> CustomerPhones { get; set; }
    }
    
    
    public class CustomerPhoneVM : BaseVM
    {
        [Required, MaxLength(16)]
        public string Phone { get; set; }
        public bool Deleted { get; set; }
        public bool Updated { get; set; }
        public bool Created { get; set; }
    }
}