﻿using System;
using System.Collections.Generic;
using WarehouseDAL.Entities;

namespace WarehouseBLL.ViewModels
{
    public class InInvoiceVM : BaseVM
    {
        public DateTime Date { get; set; }
        public Supplier Supplier { get; set; }
        public Employee Employee { get; set; }
        public IEnumerable<InItemWM> IncomeItems { get; set; }
    }

    public class InItemWM : BaseVM
    {
        public Product Product { get; set; }
        public int Count { get; set; }
        public int BuyPrice { get; set; }
        public bool Deleted { get; set; }
        public bool Updated { get; set; }
        public bool Created { get; set; }
    }
}