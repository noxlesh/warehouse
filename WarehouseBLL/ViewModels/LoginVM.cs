﻿using System.ComponentModel.DataAnnotations;

namespace WarehouseBLL.ViewModels
{
    public class LoginVM
    {
        [Required, StringLength(32)]
        public string UserName { get; set; }
        [Required, StringLength(24)]
        public string Password { get; set; }
    }
}