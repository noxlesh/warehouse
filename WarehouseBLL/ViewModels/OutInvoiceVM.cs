﻿using System;
using System.Collections.Generic;
using WarehouseDAL.Entities;

namespace WarehouseBLL.ViewModels
{
    public class OutInvoiceVM : BaseVM
    {
        public DateTime Date { get; set; }
        public Employee Employee { get; set; }
        public Customer Customer { get; set; }
        public IEnumerable<OutItemVM> OutItems { get; set; }
    }

    public class OutItemVM : BaseVM
    {
        public Product Product { get; set; }
        public int Count { get; set; }
        public int SellPrice { get; set; }
        public bool Deleted { get; set; }
        public bool Updated { get; set; }
        public bool Created { get; set; }
    }
}