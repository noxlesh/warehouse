﻿namespace WarehouseBLL.ViewModels
{
    public class ProductVM : BaseVM
    {
        
        public string Name { get; set; }
        public string MeasureName { get; set; }
        public string Notation { get; set; }
        public int UnitPrice { get; set; }
    }

    public class ProductCuVm : BaseVM
    {
        public string Name { get; set; }
        public int MeasureId { get; set; }
        public int CategoryId { get; set; }
        public string Notation { get; set; }
        public int UnitPrice { get; set; }
    }
}