﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WarehouseBLL.ViewModels
{
    public class SupplierVM : BaseVM
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public IEnumerable<SupplierPhoneVM> SupplierPhones { get; set; }
        
    }

    public class SupplierPhoneVM : BaseVM
    {
        [Required, MaxLength(16)]
        public string Phone { get; set; }
        public bool Deleted { get; set; }
        public bool Updated { get; set; }
        public bool Created { get; set; }
    }
}