﻿namespace WarehouseDAL.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}