﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("categories")]
    public class Category : BaseEntity
    {
        [Required,MaxLength(64)]
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}