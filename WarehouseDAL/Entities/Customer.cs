﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("customers")]
    public class Customer : BaseEntity
    {
        [Required,MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(128)]
        public string Address { get; set; }
        
        public IEnumerable<CustomerPhone> CustomerPhones { get; set; }
        public IEnumerable<OutInvoice> OutcomeInvoices { get; set; }
    }
}