﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("customer_phones")]
    public class CustomerPhone : BaseEntity
    {
        [Required, MaxLength(16)]
        public string Phone { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}