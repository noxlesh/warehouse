﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace WarehouseDAL.Entities
{
    [Table("employees")]
    public class Employee : IdentityUser
    {
        public IEnumerable<InInvoice> IncomeInvoices { get; set; }
        public IEnumerable<OutInvoice> OutcomeInvoices { get; set; }
    }
}