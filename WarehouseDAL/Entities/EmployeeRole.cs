﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace WarehouseDAL.Entities
{
    [Table("employee_roles")]
    public class EmployeeRole : IdentityRole
    {
        public EmployeeRole()
        {
        }
        public EmployeeRole(string roleName) : base(roleName)
        {
        }
    }
}