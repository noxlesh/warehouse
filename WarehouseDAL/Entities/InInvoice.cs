﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("income_invoice")]
    public class InInvoice : BaseEntity
    {
        [Required]
        public DateTime Date { get; set; }
        public Guid EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int SupplierId { get; set; }
        public Supplier Supplier { get; set; }
        public IEnumerable<InItem> InItems { get; set; }
    }
}