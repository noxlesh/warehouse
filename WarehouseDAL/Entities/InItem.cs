﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("income_party")]
    public class InItem : BaseEntity
    {
        public int IncomeInvoiceId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        [Required]
        public int Count { get; set; }
        [Required]
        public int BuyPrice { get; set; }
        
    }
}