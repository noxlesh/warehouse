﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("measures")]
    public class Measure : BaseEntity
    {
        [Required,MaxLength(12)]
        public string Name { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}