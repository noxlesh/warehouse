﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("outome_invoice")]
    public class OutInvoice : BaseEntity
    {
        public DateTime Date { get; set; }
        public Guid EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public IEnumerable<OutItem> OutItems { get; set; }
    }
}