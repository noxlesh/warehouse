﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("product")]
    public class Product : BaseEntity
    {
        [Required,MaxLength(64)]
        public string Name { get; set; }
        public int MeasureId { get; set; }
        public Measure Measure { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        [MaxLength(128)]
        public string Notation { get; set; }
        [Required]
        public int UnitPrice { get; set; }
        private IEnumerable<InItem> IncomeItems { get; set; }
        private IEnumerable<OutItem> OutcomeItems { get; set; }
    }
}