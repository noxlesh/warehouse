﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("suppliers")]
    public class Supplier : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        
        public IEnumerable<SupplierPhone> SupplierPhones { get; set; }
        public IEnumerable<InInvoice> IncomeInvoices { get; set; }
    }
}