﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseDAL.Entities
{
    [Table("supplier_phones")]
    public class SupplierPhone : BaseEntity
    {
        [Required, MaxLength(16)]
        public string Phone { get; set; }
        public int SupplierId { get; set; }
        public Supplier Supplier { get; set; }
    }
}