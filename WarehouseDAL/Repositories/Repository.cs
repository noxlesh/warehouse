﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;

namespace WarehouseDAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly WarehouseContext context;
        private DbSet<T> entities;
 
        public Repository(WarehouseContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }
        public IQueryable<T> GetAll()
        {
            return entities.AsQueryable();
        }
 
        public T Get(int id)
        {
            return entities.Find(id);
        }
        public void Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            SaveChange();
        }
 
        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            SaveChange();
        }
 
        public void Delete(int id)
        {
            entities.Remove(entities.Find(id));
            SaveChange();
        }
        private void SaveChange()
        {
            context.SaveChanges();
        }
    }
}