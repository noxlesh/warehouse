﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WarehouseDAL.Entities;

namespace WarehouseDAL
{
    public class WarehouseContext : IdentityDbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPhone> CustomerPhones { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeRole> EmployeeRoles { get; set; }
        public DbSet<Product> Goods { get; set; }
        public DbSet<InItem> IncomeParties { get; set; }
        public DbSet<InInvoice> IncomeInvoices { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<OutItem> OutcomeParties { get; set; }
        public DbSet<OutInvoice> OutcomeInvoices { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<SupplierPhone> SupplierPhones { get; set; }
        
        public WarehouseContext(DbContextOptions<WarehouseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}