﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;

namespace WarehouseWebAPI.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService cs)
        {
            _categoryService = cs;
        }
        
        // GET api/Category
        [HttpGet]
        public JsonResult Get()
        {
            IEnumerable<CategoryVM> cl = _categoryService.GetCategories();
            var r = new JsonResult(cl);
            if (cl == null)
                r.StatusCode = 204;
            return r;
        }

        // GET api/Category/5
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            CategoryVM c = _categoryService.GetCategory(id);
            var r = new JsonResult(c);
            if (c == null)
                r.StatusCode = 404;
            return r;
        }

        // POST api/Category
        [HttpPost]
        public void Post([FromBody] CategoryVM categoryVm)
        {
            _categoryService.CreateCategory(categoryVm);
        }

        // PUT api/Category/5
        [HttpPut("{id}")]
        public void Put(int id, CategoryVM categoryVm)
        {
            if (_categoryService.GetCategory(id) != null)
            {
                _categoryService.UpdateCategory(categoryVm);
            }
            else
            {
                _categoryService.CreateCategory(categoryVm);
            }
        }

        // DELETE api/Category/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _categoryService.DeleteCategory(id);
        }
    }
}