﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;

namespace WarehouseWebAPI.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService cs)
        {
            _customerService = cs;
        }
        
        // GET api/customer
        [HttpGet]
        public JsonResult Get()
        {
            IEnumerable<CustomerVM> cl = _customerService.GetCustomers();
            var r = new JsonResult(cl);
            if (cl == null)
                r.StatusCode = 204;
            return r;
        }

        // GET api/customer/5
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            CustomerVM c = _customerService.GetCustomer(id);
            var r = new JsonResult(c);
            if (c == null)
                r.StatusCode = 404;
            return r;
        }

        // POST api/customer
        [HttpPost]
        public void Post([FromBody] CustomerVM customerVm)
        {
            _customerService.CreateCustomer(customerVm);
        }

        // PUT api/customer/5
        [HttpPut("{id}")]
        public void Put(int id, CustomerVM customerVm)
        {
            if (_customerService.GetCustomer(id) != null)
            {
                _customerService.UpdateCustomer(customerVm);
            }
            else
            {
                _customerService.CreateCustomer(customerVm);
            }
        }

        // DELETE api/customer/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _customerService.DeleteCustomer(id);
        }
    }
}