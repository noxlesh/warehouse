﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;

namespace WarehouseWebAPI.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/[controller]")]
    public class InInvoiceController : Controller
    {
        private readonly IInInvoiceService _inInvoiceService;

        public InInvoiceController(IInInvoiceService inInvoiceService)
        {
            _inInvoiceService = inInvoiceService;
        }
        
        // GET api/IncomeInvoice
        [HttpGet]
        public JsonResult Get()
        {
            IEnumerable<InInvoiceVM> iil = _inInvoiceService.GetIncomeInvoices();
            var r = new JsonResult(iil);
            if (iil == null)
                r.StatusCode = 204;
            return r;
        }

        // GET api/IncomeInvoice/5
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            InInvoiceVM ii = _inInvoiceService.GetIncomeInvoice(id);
            var r = new JsonResult(ii);
            if (ii == null)
                r.StatusCode = 404;
            return r;
        }

        // POST api/IncomeInvoice
        [HttpPost]
        public void Post([FromBody] InInvoiceVM inInvoiceVm)
        {
            Guid employeeId = Guid.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            _inInvoiceService.CreateIncomeInvoice(employeeId, inInvoiceVm);
        }

        // PUT api/IncomeInvoice/5
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] InInvoiceVM inInvoiceVm)
        {
            if (_inInvoiceService.GetIncomeInvoice(id) != null)
            {
                _inInvoiceService.UpdateIncomeInvoice(inInvoiceVm);
            }
            else
            {
                Guid employeeId = Guid.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
                _inInvoiceService.CreateIncomeInvoice(employeeId, inInvoiceVm);
            }
        }

        // DELETE api/IncomeInvoice/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _inInvoiceService.DeleteIncomeInvoice(id);
        }
    }
}