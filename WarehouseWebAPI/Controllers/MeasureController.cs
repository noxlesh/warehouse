﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;

namespace WarehouseWebAPI.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/[controller]")]
    public class MeasureController : Controller
    {
        private readonly IMeasureService _measureService;

        public MeasureController(IMeasureService ms)
        {
            _measureService = ms;
        }
        
        // GET api/measure
        [HttpGet]
        public JsonResult Get()
        {
            
            IEnumerable<MeasureVM> ml = _measureService.GetMeasures();
            var r = new JsonResult(ml);
            if (ml == null)
                r.StatusCode = 204;
            return r;
        }

        // GET api/measure/5
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            MeasureVM m = _measureService.GetMeasure(id);
            var r = new JsonResult(m);
            if (m == null)
                r.StatusCode = 404;
            return r;
        }

        // POST api/measure
        [HttpPost]
        public void Post([FromBody] MeasureVM measureVm)
        {
            _measureService.CreateMeasure(measureVm);
        }

        // PUT api/measure/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] MeasureVM measureVm)
        {
            if (_measureService.GetMeasure(id) != null)
            {
                _measureService.UpdateMeasure(measureVm);
            }
            else
            {
                _measureService.CreateMeasure(measureVm);
            }
        }

        // DELETE api/measure/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _measureService.DeleteMeasure(id);
        }
    }
}