﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;

namespace WarehouseWebAPI.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/[controller]")]
    public class OutInvoiceController : Controller
    {
        private readonly IOutInvoiceService _outInvoiceService;

        public OutInvoiceController(IOutInvoiceService outInvoiceService)
        {
            _outInvoiceService = outInvoiceService;
        }
        
        // GET api/OutcomeInvoice
        [HttpGet]
        public JsonResult Get()
        {
            IEnumerable<OutInvoiceVM> oil = _outInvoiceService.GetOutcomeInvoices();
            var r = new JsonResult(oil);
            if (oil == null)
                r.StatusCode = 204;
            return r;
        }

        // GET api/OutcomeInvoice/5
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            OutInvoiceVM oi = _outInvoiceService.GetOutcomeInvoice(id);
            var r = new JsonResult(oi);
            if (oi == null)
                r.StatusCode = 404;
            return r;
        }

        // POST api/OutcomeInvoice
        [HttpPost]
        public void Post([FromBody] OutInvoiceVM outInvoiceVm)
        {
            Guid employeeId = Guid.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            _outInvoiceService.CreateOutcomeInvoice(employeeId, outInvoiceVm);
        }

        // PUT api/OutcomeInvoice/5
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] OutInvoiceVM outInvoiceVm)
        {
            if (_outInvoiceService.GetOutcomeInvoice(id) != null)
            {
                _outInvoiceService.UpdateOutcomeInvoice(outInvoiceVm);
            }
            else
            {
                Guid employeeId = Guid.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
                _outInvoiceService.CreateOutcomeInvoice(employeeId, outInvoiceVm);
            }
        }

        // DELETE api/OutcomeInvoice/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _outInvoiceService.DeleteOutcomeInvoice(id);
        }
    }
}