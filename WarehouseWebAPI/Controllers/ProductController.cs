﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;

namespace WarehouseWebAPI.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService ps)
        {
            _productService = ps;
        }
        
        // GET api/product
        [HttpGet]
        public JsonResult Get()
        {
            IEnumerable<ProductVM> pl = _productService.GetProducts();
            var r = new JsonResult(pl);
            if (pl == null)
                r.StatusCode = 204;
            return r;
        }

        // GET api/product/5
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            ProductVM p = _productService.GetProduct(id);
            var r = new JsonResult(p);
            if (p == null)
                r.StatusCode = 404;
            return r;
        }

        // POST api/product
        [HttpPost]
        public void Post([FromBody] ProductCuVm productCuVm)
        {
            _productService.CreateProduct(productCuVm);
        }

        // PUT api/product/5
        [HttpPut("{id}")]
        public void Put(int id, ProductCuVm productCuVm)
        {
            if (_productService.GetProduct(id) != null)
            {
                _productService.UpdateProduct(productCuVm);
            }
            else
            {
                _productService.CreateProduct(productCuVm);
            }
        }

        // DELETE api/product/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _productService.DeleteProduct(id);
        }
    }
}