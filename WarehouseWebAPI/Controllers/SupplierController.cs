﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WarehouseBLL.Interfaces;
using WarehouseBLL.ViewModels;

namespace WarehouseWebAPI.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/[controller]")]
    public class SupplierController : Controller
    {
        private readonly ISupplierService _supplierService;

        public SupplierController(ISupplierService ss)
        {
            _supplierService = ss;
        }
        
        // GET api/supplier
        [HttpGet]
        public JsonResult Get()
        {
            IEnumerable<SupplierVM> sl = _supplierService.GetSuppliers();
            var r = new JsonResult(sl);
            if (sl == null)
                r.StatusCode = 204;
            return r;
        }

        // GET api/supplier/5
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            SupplierVM s = _supplierService.GetSupplier(id);
            var r = new JsonResult(s);
            if (s == null)
                r.StatusCode = 404;
            return r;
        }

        // POST api/supplier
        [HttpPost]
        public void Post([FromBody] SupplierVM supplierVm)
        {
            _supplierService.CreateSupplier(supplierVm);
        }

        // PUT api/supplier/5
        [HttpPut("{id}")]
        public void Put(int id, SupplierVM supplierVm)
        {
            if (_supplierService.GetSupplier(id) != null)
            {
                _supplierService.UpdateSupplier(supplierVm);
            }
            else
            {
                _supplierService.CreateSupplier(supplierVm);
            }
        }

        // DELETE api/supplier/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _supplierService.DeleteSupplier(id);
        }
    }
}