﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using WarehouseBLL.Interfaces;
using WarehouseBLL.Services;
using WarehouseDAL;
using WarehouseDAL.Entities;
using WarehouseDAL.Interfaces;
using WarehouseDAL.Repositories;

namespace WarehouseWebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<WarehouseContext>(options => options.UseMySql(Configuration.GetConnectionString("MysqlConnection")));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IMeasureService, MeasureService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ISupplierService, SupplierService>();
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<IInInvoiceService, InInvoiceService>();
            services.AddTransient<IOutInvoiceService, IOutInvoiceService>();
            
            services.AddIdentity<Employee, EmployeeRole>()
                .AddEntityFrameworkStores<WarehouseContext>()
                .AddDefaultTokenProviders();
            
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });
            
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc();
            CreateRoles(serviceProvider).Wait();
            CreateAdmin(serviceProvider).Wait();
        }
       
        // Creates user roles if they not exists
        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<EmployeeRole>>();
            foreach (var roleName in new []{ "Admin", "Manager"})
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    await roleManager.CreateAsync(new EmployeeRole (roleName));
                }
            }
        }

        // Creates an admin user if it not exists
        private async Task CreateAdmin(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<Employee>>();
            var adminUser = new Employee
            {
                UserName = Configuration["AdminUserName"],
                Email = Configuration["AdminUserEmail"]
            };
            string adminUserPwd = Configuration["AdminUserPassword"];
            
            // checks if the admin user already exists
            if(await userManager.FindByEmailAsync(Configuration["AdminUserEmail"]) == null)
            {
                var createPowerUser = await userManager.CreateAsync(adminUser, adminUserPwd);
                if (createPowerUser.Succeeded)
                {
                    // ties the new user to the role
                    await userManager.AddToRoleAsync(adminUser, "Admin");

                }
            }
        }
    }
}